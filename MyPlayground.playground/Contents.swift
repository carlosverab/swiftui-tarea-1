import UIKit

var greeting = "Hello, playground"


struct Operacion{
    var simbolo: String
    
    func operar(a: String, b: String){
        var bOperacionCorrecta = true
        
        // valida si variables son numéricas
        
        guard let x = Double(a) else {
            print("variable a = \(a) no es numérica")
            
            if Double(b) == nil {
                print("variable b = \(b) no es numérica")
            }
            
            return
        }
        
        guard let z = Double(b) else {
            print("variable b = \(b) no es numérica")
            return
        }
        
        // valida si no es división entre 0
        
        guard !(z == 0.0 && simbolo == "/") else {
            print("Operación inválida, por division entre cero")
            return
        }
        
        
        var resultado: Double = 0
        
        switch simbolo {
            case "+":
                resultado = x + z
            case "-":
                resultado = x - z
            case "*":
                resultado = x * z
            case "/":
                resultado = x / z
            case "p":
                resultado = pow(x, z)
            default:
                bOperacionCorrecta = false
        }
        
        if bOperacionCorrecta{
            
            // quitar la parte decimal si el número es un entero
            
            let entero = Int(trunc(resultado))
            
            if (resultado - Double(entero)) == 0 {
                print("El resultado es: \(entero)")
            } else {
                print("El resultado es: \(resultado)")
            }
            
        } else {
            
            print("Operación desconocida")

        }
    }
}

let operacion = Operacion(simbolo: "*")
operacion.operar(a: "2.8u", b: "23.30")
